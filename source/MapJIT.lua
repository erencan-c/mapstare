local ffi = require('ffi')

---@class Pair
	---@field x integer
	---@field y integer
	---@field row integer
	---@field col integer

---@class Pop
	---@field amount integer
	---@field occupation string
	---@field religion string
	---@field nationality string
	---@field literacy integer
	---@field needsFullified integer

---@class Tile
	---@field popNumber integer
	---@field type integer
	---@field pops Pop[]

---@class Map
	---@field row integer
	---@field col integer
	---@field hovered Pair
	---@field clicked Pair
	---@field tiles Tile[]

ffi.cdef[[
	typedef enum {
		TTILE_DEEPOCEAN = -4,
		TTILE_OCEAN,
		TTILE_SEA,
		TTILE_SHORE,

		TTILE_BEACH,
		TTILE_PLAINS,
		TTILE_HILLS,
		TTILE_MOUNTAIN
	} Tiletype;

	typedef union {
		struct {
			size_t x, y;
		};
		struct {
			size_t row, col;
		}
	} Pair;

	typedef struct {
		uint64_t amount;
		char occupation[4];
		char religion[4];
		char nationality[4];
		uint16_t literacy;
		uint16_t needsFullified;
	} Pop;

	typedef struct {
		size_t popNumber;
		Tiletype type;
		Pop pops[?];
	} Tile;

	typedef struct Map {
		Pair position;
		Pair size;
		Pair blockSize;
		Pair hovered;
		Pair clicked;
		Tile* tiles[?];
	} Map;
]]

---@class Map
local map_mt = {}

function map_mt:update(dt)
	
end

function map_mt:mousemoved(x, y, dx, dy, istouch)
	
end

map_mt.__index = map_mt

print(ffi.C.TTILE_SEA)
