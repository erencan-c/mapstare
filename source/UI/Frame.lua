local UIElement = require('source.UI.UIElement')

local max = math.max

---@class GridData
	---@field row integer
	---@field col integer
	---@field cellWidth integer
	---@field cellHeight integer

---@class Border
	---@field thickness number
	---@field color Color

---@class Frame : UIElement
local Frame = UIElement:extend('UI::Frame')
---@class Frame
	---@field children UIElement[]
	---@field packingType "'grid'"
	---@field gridData GridData
	---@field margin {horizontal: number, vertical: number}
	---@field border {bottom: Border, left: Border, right: Border, top: Border}
	---@field padding number
	---@field width number
	---@field height number

---
---@param self Frame
---@param args Frame
local function initGrid(self, args)
	self.gridData = args.gridData or {
		row = 1,
		col = 1,
		cellWidth = 0,
		cellHeight = 0
	}
	if args.gridData then
		self.gridData.row = args.gridData.row or 0
		self.gridData.col = args.gridData.col or 0
		self.gridData.cellWidth = args.gridData.cellWidth or 0
		self.gridData.cellHeight = args.gridData.cellHeight or 0
	end
	self.margin = args.margin or {horizontal = 0, vertical = 0}
	if args.margin then
		self.margin.horizontal = args.margin.horizontal or 0
		self.margin.vertical = args.margin.vertical or 0
	end
	
	local emptyBorder = {thickness = 0, color = {0,0,0,1}} ---@type Border

	self.border = args.border or {bottom = emptyBorder, left = emptyBorder, right = emptyBorder, top = emptyBorder}
	if args.border then
		self.border.left = args.border.left or args.border.right or emptyBorder
		self.border.right = args.border.right or args.border.left or emptyBorder
		self.border.top = args.border.top or args.border.bottom or emptyBorder
		self.border.bottom = args.border.bottom or args.border.top or emptyBorder

		if args.border.left then
			self.border.left.thickness = self.border.left.thickness or self.border.right.thickness or 0
			self.border.right.color = self.border.right.color or self.border.left.color or {0,0,0,1}
		end
		if args.border.right then
			self.border.right.thickness = self.border.right.thickness or self.border.left.thickness or 0
			self.border.left.color = self.border.left.color or self.border.right.color or {0,0,0,1}
		end
		if args.border.top then
			self.border.top.thickness = self.border.top.thickness or self.border.bottom.thickness or 0
			self.border.bottom.color = self.border.bottom.color or self.border.top.color or {0,0,0,1}
		end
		if args.border.bottom then
			self.border.bottom.thickness = self.border.bottom.thickness or self.border.top.thickness or 0
			self.border.top.color = self.border.top.color or self.border.bottom.color or {0,0,0,1}
		end
	end

	self.padding = args.padding or 0

	self.children = args.children or {}
	for i=1,self.gridData.row do
		for j=1,self.gridData.col do
			self.gridData.cellWidth = max(self.gridData.cellWidth, self.children[(i-1)*self.gridData.col + j].width or 0)
			self.gridData.cellHeight = max(self.gridData.cellHeight, self.children[(i-1)*self.gridData.col + j].height or 0)
		end
	end

	self.x = args.x or (self.children[1].x - self.margin.horizontal)
	self.y = args.y or (self.children[1].y - self.margin.vertical)

	self.width =
		2*self.margin.horizontal + self.border.left.thickness + self.border.right.thickness +
		self.gridData.cellWidth*self.gridData.col + self.padding*(self.gridData.col-1)
	self.height =
		2*self.margin.vertical + self.border.top.thickness + self.border.bottom.thickness +
		self.gridData.cellHeight*self.gridData.row + self.padding*(self.gridData.row-1)
end

---@param args Frame
function Frame:init(args)
	if args.packingType == 'grid' then
		initGrid(self, args)
	end
end

function Frame:update(dt)
	for i=1,self.gridData.row do
		for j=1,self.gridData.col do
			if self.children[(i-1)*self.gridData.col + j] then
				self.children[(i-1)*self.gridData.col + j]:update(dt)
			end
		end
	end
end

function Frame:draw()
	if self.border.left.thickness ~= 0 then
		love.graphics.rectangle('fill', self.x, self.y, self.border.left.thickness, self.height)
	end
end

return Frame
