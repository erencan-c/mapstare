local insert = table.insert
local abs = math.abs

math.randomseed(os.time())

local utils = {}

---Apply function
---@param iterable any[]
---@param func function|string if string, the argument is `x`
---@param len? integer
function utils.apply(iterable, func, len)
	if type(func) == 'string' then func = loadstring(('return function(x) return (%s) end'):format(func))() end
	len = len or #iterable
	for i=1,len do
		iterable[i] = func(iterable[i])
	end
end

---Map function
---@param iterable any[]
---@param func function|string if string, the argument is `x`
---@param len? integer
---@return WrappedArray mapped
function utils.map(iterable, func, len)
	if type(func) == 'string' then func = loadstring(('return function(x) return (%s) end'):format(func))() end
	len = len or #iterable
	local ret = utils.arraywrapper{}
	for i=1,len do
		ret[i] = func(iterable[i])
	end
	return ret
end

---Filter function
---@param iterable any[]
---@param func function|string if string, the argument is `x`
---@param len? integer
---@return WrappedArray filtered
function utils.filter(iterable, func, len)
	if type(func) == 'string' then func = loadstring(('return function(x) return (%s) end'):format(func))() end
	len = len or #iterable
	local ret = utils.arraywrapper{}
	for i=1,len do
		if func(iterable[i]) then
			insert(ret, iterable[i])
		end
	end
	return ret
end

---Reduce function
---@param iterable any[]
---@param func function|string if string, name of total is `total` and the argument is `x`
---@param len? integer
---@return any reduced
function utils.reduce(iterable, func, len)
	if type(func) == 'string' then func = loadstring(('return function(total, x) return (%s) end'):format(func))() end
	len = len or #iterable
	local reduced = iterable[1]
	for i=2,len do
		reduced = func(reduced, iterable[i])
	end
	return reduced
end

---Do `func` on each element of `iterable`
---@param iterable any[]
---@param func function|string if string, the argument is `x`
---@param len? integer
function utils.each(iterable, func, len)
	if type(func) == 'string' then func = loadstring(('return function(x) return (%s) end'):format(func))() end
	len = len or #iterable
	for i=1,len do
		func(iterable[i])
	end
end

---Select a random item from the given iterable
---@param iterable any[]
---@return any randomized
function utils.randomSelect(iterable)
	local min = 1
	local max = #iterable
	return iterable[math.random(min, max)]
end

function utils.copy(iterable)
	return utils.map(iterable, function(x) return x end)
end

---@class WrappedArray
local wrap_mt = {
	map = utils.map,
	apply = utils.apply,
	filter = utils.filter,
	reduce = utils.reduce,
	each = utils.each,
	randomSelect = utils.randomSelect,

}
wrap_mt.__index = wrap_mt

function utils.arraywrapper(iterable)
	return setmetatable(iterable, wrap_mt)
end

---Clamp `value` between `lowerBound` and `upperBound`, defaults to [0,1]
---@param value number
---@param lowerBound? number
---@param upperBound? number
---@return number clamped
function utils.clamp(value, lowerBound, upperBound)
	lowerBound = lowerBound or 0
	upperBound = upperBound or 1
	return (value < lowerBound and lowerBound) or (value > upperBound and upperBound) or value
end

local clamp = utils.clamp

---Wrap a value if it is out of bounds of [`lowerBound`, `upperBound`], defaults to [0,1]
---@param value number
---@param lowerBound? number
---@param upperBound? number
---@return number wrapped
function utils.wrap(value, lowerBound, upperBound)
	lowerBound = lowerBound or 0
	upperBound = upperBound or 1
	return abs(value - lowerBound) % (upperBound - lowerBound) + lowerBound
end

---Interpolate a value linearly on [`lowerBound`, `upperBound`]
---@param value number
---@param lowerBound number
---@param upperBound number
---@return number interpolated
function utils.interpolate(value, lowerBound, upperBound)
	return (clamp(value, lowerBound, upperBound) - lowerBound) / (upperBound - lowerBound)
end

---Create a Conway's Game of Life grid `width`x`height` in `iteration` iterations
---@param width integer
---@param height integer
---@param iteration integer
---@return WrappedArray
function utils.conway(width, height, iteration, rule)
	local ret = {width = width, height = height, iteration = 0, rule = rule} ---@type WrappedArray
	local nextGen = {width = width, height = height, iteration = 0, rule = rule} ---@type WrappedArray
	for i=1,width do
		for j=1,height do
			local rand = love.math.random(0,1)
			ret[(i-1)*height + j] = rand
		end
	end

	for i=1,width do
		for j=1,height do
			nextGen[(i-1)*height + j] = rule(ret, i, j, width, height)
		end
	end
	ret, nextGen = nextGen, ret
	return utils.arraywrapper(ret)
end

function utils.contconway(prev)
	local ret = {width = prev.width, height = prev.height, iteration = prev.iteration+1, rule = prev.rule}
	for i=1,prev.width do
		for j=1,prev.height do
			ret[(i-1)*ret.height + j] = prev.rule(prev, i, j, prev.width, prev.height)
		end
	end
	return ret
end

local oldToString = tostring

local function tostringhelper(self, depth)
	if type(self) ~= 'table' then
		return oldToString(self)
	end
	local buf = {'{ '}
	local len = #self
	if len == 0 then -- hash table
		insert(buf, '\n')
		for k,v in pairs(self) do
			insert(buf, ("%s%s = %s,\n"):format(string.rep('   ', depth), tostringhelper(k,depth+1), tostringhelper(v,depth+1)))
		end
	else -- array
		for i=1,len do
			insert(buf, ('%s, '):format(tostringhelper(self[i],depth)))
		end
	end
	insert(buf, ('%s}'):format(string.rep('   ', depth-1)))
	return table.concat(buf)
end

-- function tostring(v)
-- 	return tostringhelper(v, 1)
-- end

return utils