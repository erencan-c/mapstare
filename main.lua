local Map = require('source.Map')
local UI = require('source.UI')
local mat4 = require('source.mat4')

local map ---@type Map
local font ---@type love.Font
local button ---@type Button
local frame ---@type Frame

local sin = math.sin
local cos = math.cos

function love.load()
	local start = os.clock()
	love.window.setVSync(-1)
	map = Map.new(800, 600, 0, 0, 1, 400, 400)
	font = love.graphics.getFont()
	button = UI.Button:new{
		x = 0,
		y = 550,
		width = 100,
		height = 50,
		text = 'Hello, World!',
		color = {0.80, 0, 0, 1},
		font = font,
		shader = love.graphics.newShader([[
			vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords) {
				return vec4(texture_coords.x/800, texture_coords.y/600, (screen_coords.x/screen_coords.y)/(800/600), 1.0);
			}
		]]),
		onclick = function(self, mouseButton)
			print(mouseButton)
		end,
		onhover = function(self) ---@param self Button
			local sourceColor = {0.90, 0, 0, 1}
			local targetColor = {0.20, 0.50, 0.50, 1}

			local xChange = 0
			local yChange = -550
			local widthChange = 700
			local heightChange = 550
			local colorChange = {targetColor[1] - sourceColor[1], targetColor[2] - sourceColor[2], targetColor[3] - sourceColor[3], targetColor[4] - sourceColor[4]}
			table.insert(self.animations, {
				change = function(btn, dt, remaining)
					btn.color[1] = btn.color[1] + colorChange[1]*dt
					btn.color[2] = btn.color[2] + colorChange[2]*dt
					btn.color[3] = btn.color[3] + colorChange[3]*dt
					btn.color[4] = btn.color[4] + colorChange[4]*dt

					btn.x = btn.x + xChange*dt
					btn.y = btn.y + yChange*dt
					btn.width = btn.width + widthChange*dt
					btn.height = btn.height + heightChange*dt
				end,
				duration = 1
			})
		end,
		outhover = function(self) ---@type fun(self: Button)
			local sourceColor = {0.20, 0.50, 0.50, 1}
			local targetColor = {0.90, 0, 0, 1}
			local colorChange = {targetColor[1] - sourceColor[1], targetColor[2] - sourceColor[2], targetColor[3] - sourceColor[3], targetColor[4] - sourceColor[4]}

			local xChange = 0
			local yChange = 550
			local widthChange = -700
			local heightChange = -550
			table.insert(self.animations, {
				change = function(btn, dt, remaining)
					btn.color[1] = btn.color[1] + colorChange[1]*dt
					btn.color[2] = btn.color[2] + colorChange[2]*dt
					btn.color[3] = btn.color[3] + colorChange[3]*dt
					btn.color[4] = btn.color[4] + colorChange[4]*dt

					btn.x = btn.x + xChange*dt
					btn.y = btn.y + yChange*dt
					btn.width = btn.width + widthChange*dt
					btn.height = btn.height + heightChange*dt
				end,
				duration = 1
			})
		end,
		align = 'center'
	}
	frame = UI.Frame:new{
		packingType = 'grid',
		children = {button},
		margin = {horizontal = 10},
		border = {
			left = {thickness = 10}
		}
	}
end

function love.update(dt)
	map:update(dt)
	frame:update(dt)
end

local points = {
	mat4.point(350, 250, 50),
	mat4.point(350, 350, 50),
	mat4.point(450, 350, 50),
	mat4.point(450, 250, 50),

	mat4.point(350, 250, 150),
	mat4.point(350, 350, 150),
	mat4.point(450, 350, 150),
	mat4.point(450, 250, 150)
}

local theta = math.pi/2.1

local rotateX = mat4.new{{
	{1, 0, 0, 0},
	{0, cos(theta), sin(theta)},
	{0, -sin(theta), cos(theta)},
	{0,0,0,1}
}}

local rotateY = mat4.new{{
	{cos(theta), 0, -sin(theta)},
	{0,1,0},
	{sin(theta), 0, cos(theta)},
	{0,0,0,1}
}}

local rotateZ = mat4.new{{
	{cos(theta), sin(theta)},
	{-math.sin(theta), cos(theta)},
	{0,0,1},
	{0,0,0,1}
}}

function love.draw()
	map:draw()
	button:draw()
	love.graphics.print(tostring(frame.y), 0, 0)
end

function love.wheelmoved(x, y)
	map:wheelmoved(x, y)
end

function love.mousemoved(x, y, dx, dy, istouch)
	map:mousemoved(x, y, dx, dy, istouch)
	button:mousemoved(x, y, dx, dy, istouch)
end

function love.mousepressed(x, y, mouseButton, istouch, presses)
	map:mousepressed(x, y, mouseButton, istouch, presses)
	button:mousepressed(x, y, mouseButton, istouch, presses)
end

function love.mousereleased(x, y, mouseButton, istouch, presses)
	map:mousereleased(x, y, mouseButton, istouch, presses)
end
