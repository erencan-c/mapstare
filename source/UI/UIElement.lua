local class = require('source.30log')


---@class UIElement
local UIElement = class('UIElement')
---@class UIElement
	---@field x integer
	---@field y integer
	---@field animations Animation[]
	---@field shader love.Shader


function UIElement:init(args)
	self.x = args.x or 0
	self.y = args.y or 0
	self.animations = args.animations or {} ---@type Animation[]
	self.shader = args.shader ---@type love.Shader
end

function UIElement:update(dt)
	local animationListLength = #self.animations
	if animationListLength ~= 0 then
		for i=animationListLength,1,-1 do
			self.animations[i].change(self, dt, self.animations[i].duration)
			self.animations[i].duration = self.animations[i].duration - dt
			if self.animations[i].duration <= 0 then
				table.remove(self.animations, i)
			end
		end
	end
end

return UIElement