local utils = require('source.utils')
local Tile = require('source.Tile')

local function gameoflife(data, x, y, width, height)
	local sum = 0
	local cell = data[(x-1)*height + y]
	for i=-2,2 do
		for j=-2,2 do
			local neigh = data[(x+i-1)*height + (y+j)]
			if neigh == nil then
				local u = i % width + 1
				local v = j % height + 1
				neigh = data[(u-1)*height + v]
			end
			sum = sum + neigh
		end
	end
	sum = sum - cell
	if cell == 1 then
		if sum >= 2 and sum <= 7 then
			return 1
		else
			return 0
		end
	else
		if sum == 6 then
			return 1
		else
			return 0
		end
	end
end

---@class Map
local Map = {}
Map.__index = Map

---@param width integer
---@param height integer
---@param x? number
---@param y? number
---@param cellSize? integer
---@param noiseScaleX? number
---@param noiseScaleY? number
---@return Map
function Map.new(width, height, x, y, cellSize, noiseScaleX, noiseScaleY)
	local seed = love.math.random()
	noiseScaleX = noiseScaleX or 100
	noiseScaleY = noiseScaleY or noiseScaleX
	---@class Map
	local ret = {
		width = width,
		height = height,
		cellSize = cellSize or 10,
		x = x or 0,
		y = y or 0,
		scroll = 1,
		dragged = false,
		hover = {0, 0},
		clicked = {0, 0},
		canvas = love.graphics.newCanvas(width*cellSize, height*cellSize),
		data = {}, ---@type Tile[]
		conw = utils.conway(width, height, 0, gameoflife),
	}
	love.graphics.setCanvas(ret.canvas)
	love.graphics.clear()
	love.graphics.setBlendMode('alpha')
	for i=1,width do
		for j=1,height do
			local rand = love.math.noise((i-1)/noiseScaleX, (j-1)/noiseScaleY)
			local tileType = ''
			if rand < 0.3 then
				tileType = 'ocean'
			elseif rand < 0.5 then
				tileType = 'shallow sea'
			elseif rand < 0.9 then
				tileType = 'plains'
			else
				tileType = 'mountains'
			end
			Map.setTile(ret, i, j, Tile.new(tileType))
			local color = Map.getTile(ret, i, j):color()
			love.graphics.setColor(color)
			love.graphics.rectangle('fill', (i-1)*cellSize, (j-1)*cellSize, cellSize, cellSize)
		end
	end
	love.graphics.setCanvas()
	ret.canvas:setFilter('nearest')

	return setmetatable(ret, Map)
end

---@param x integer
---@param y integer
---@return Tile
function Map:getTile(x, y)
	return self.data[Map.index(self, x, y)]
end

---@param x integer
---@param y integer
---@param tile Tile
function Map:setTile(x, y, tile)
	self.data[Map.index(self, x, y)] = tile
end

function Map:index(x, y)
	return (x-1)*self.height + y
end

function Map:getTileUnder(x, y)
	local dx = x - self.x
	local dy = y - self.y
	return math.ceil(dx/self.cellSize), math.ceil(dy/self.cellSize)
end

function Map:update(dt)
	-- self.conw = utils.contconway(self.conw)
end

function Map:draw()
	local oldColor = {love.graphics.getColor()}
	love.graphics.setColor(1,1,1,1)
	love.graphics.draw(self.canvas, self.x, self.y, 0, self.scroll)

	if self.hover[1] ~= 0 then
		love.graphics.setColor(0,0,0,0.25)
		love.graphics.rectangle('fill', (self.hover[1]-1)*self.cellSize + self.x,
			(self.hover[2]-1)*self.cellSize + self.y, self.cellSize, self.cellSize)
	end

	if self.clicked[1] ~= 0 then
		love.graphics.setColor(0,0,0,0.5)
		love.graphics.rectangle('fill', (self.clicked[1]-1)*self.cellSize + self.x,
			(self.clicked[2]-1)*self.cellSize + self.y, self.cellSize, self.cellSize)
	end

	love.graphics.setColor(oldColor)
end

function Map:mousemoved(x, y, dx, dy, istouch)
	if self.dragged then
		self.x = self.x + dx
		self.y = self.y + dy
	else
		local mouseTile = {self:getTileUnder(x, y)}
		if (mouseTile[1] > 0 and mouseTile[1] <= self.width) and (mouseTile[2] > 0 and mouseTile[2] <= self.height) then
			self.hover[1] = mouseTile[1]
			self.hover[2] = mouseTile[2]
		else
			self.hover[1] = 0
			self.hover[2] = 0
		end
	end
end

function Map:mousepressed(x, y, button, istouch, presses)
	if button == 3 then
		self.dragged = true
	end
	if button == 1 then
		local mouseTile = {self:getTileUnder(x,y)}
		if (mouseTile[1] > 0 and mouseTile[1] <= self.width) and (mouseTile[2] > 0 and mouseTile[2] <= self.height) then
			self.clicked[1] = mouseTile[1]
			self.clicked[2] = mouseTile[2]
		else
			self.clicked[1] = 0
			self.clicked[2] = 0
		end
	end
end

function Map:mousereleased(x, y, button, istouch, presses)
	if button == 3 then
		self.dragged = false
	end
end

function Map:wheelmoved(x, y)
	local mousePos = {love.mouse.getPosition()}
	local ratio = 1 + y/10
	self.scroll = self.scroll * ratio
	local oldCellSize = self.cellSize
	self.cellSize = self.cellSize * ratio
	local mouseDist = {mousePos[1] - self.x, mousePos[2] - self.y}
	local dx = ((self.cellSize - oldCellSize) * (mouseDist[1]/(oldCellSize)))
	local dy = ((self.cellSize - oldCellSize) * (mouseDist[2]/(oldCellSize)))
	self.x = self.x - dx
	self.y = self.y - dy
end


return Map
