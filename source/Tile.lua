local utils = require('source.utils')

---@alias terrainType "plains"|"mountains"|"sea"

---@type table<terrainType, number[]>
local colorMap = {
	['plains'] = {0, 1, 0},
	['mountains'] = {0.5, 0.25, 0},
	['shallow sea'] = {0, 0.5, 1},
	['ocean'] = {0, 0, 1},
}

---@class Tile
local Tile = {}
Tile.__index = Tile

---@param terrain "plains"|"mountains"|"sea"
---@return Tile
function Tile.new(terrain)
	---@class Tile
	local ret = {
		terrain = terrain
	}
	return setmetatable(ret, Tile)
end

function Tile:color()
	return colorMap[self.terrain]
end

return Tile
