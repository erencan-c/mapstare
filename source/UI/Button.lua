local UIElement = require('source.UI.UIElement') ---@type UIElement

---@alias Animation {change: fun(self: Button, dt: number, remaining: number), duration: number}

---@alias Color number[]

---@class Button : UIElement
local Button = UIElement:extend('Button')
---@class Button : UIElement
	---@field width integer
	---@field height integer
	---@field text string
	---@field alignment "'left'"|"'right'"|"'center'"
	---@field color Color
	---@field font love.Font
	---@field textWidth number
	---@field textHeight number
	---@field textColor Color
	---@field onclick fun(self: Button)
	---@field onhover fun(self: Button)
	---@field outhover fun(self: Button)

local function emptyFunc()end

---Creates a `Button`
---@param args Button
function Button:init(args)
	Button.super.init(self, args)
	self.width = args.width or 0
	self.height = args.height or 0
	self.text = args.text or ""
	self.alignment = args.alignment or 'center'
	self.color = args.color or {0,0,0,1}
	self.font = args.font or love.graphics.getFont()
	self.textWidth = self.font:getWidth(self.text)
	self.textHeight = self.font:getHeight()
	self.textColor = args.textColor or {1,1,1,1}
	self.onhover = args.onhover or emptyFunc
	self.outhover = args.outhover or emptyFunc
	self.onclick = args.onclick or emptyFunc
end

---Checks whether the given point is inside the button
---@param x number
---@param y number
---@return boolean
function Button:isInside(x, y)
	return (x > self.x and x < self.x+self.width) and (y > self.y and y < self.y+self.height)
end

function Button:draw()
	local oldShader = love.graphics.getShader()
	if self.shader then
		love.graphics.setShader(self.shader)
	end
	local textX, textY = ({ ---@type number
		center = function()
			return self.x + self.width/2 - self.textWidth/2, self.y + self.height/2 - self.textHeight/2
		end,
		left = function()
			return self.x, self.y
		end,
		right = function()
			return self.x + self.width - self.textWidth, self.y + self.height - self.textHeight
		end
	})[self.alignment]()
	love.graphics.setColor(self.color)
	love.graphics.rectangle('fill', self.x, self.y, self.width, self.height)
	love.graphics.setShader(oldShader)
	love.graphics.setColor(self.textColor)
	love.graphics.print(self.text, textX, textY)
end


function Button:mousepressed(x, y, button, istouch, presses)
	if self:isInside(x, y) then
		self:onclick(button)
	end
end

function Button:mousemoved(x, y, dx, dy, istouch)
	if not self.hovered and self:isInside(x, y) then
		self:onhover()
		self.hovered = true
	elseif self.hovered and not self:isInside(x, y) then
		self:outhover()
		self.hovered = false
	end
end

return Button
