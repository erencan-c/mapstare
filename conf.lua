
---@param t love.conf
function love.conf(t)
	t.title = 'Mapstare'
	t.vsync = 1
	t.window.width = 800
	t.window.height = 600
	t.console = true
end