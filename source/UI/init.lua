local path = ...

---@class UI
local UI = {
	Button = require(path..'.Button'), ---@type Button
	Frame = require(path..'.Frame'), ---@type Frame
}
UI.__index = UI

return UI
