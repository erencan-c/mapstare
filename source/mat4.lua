local ffi = require('ffi')

ffi.cdef[[
	typedef struct mat4 {
		float data[4][4];
	} mat4;
]]

local sin, cos = math.sin, math.cos

---@class mat4
	---@field data number[][]
local mat4 = {}
mat4.__index = mat4

---
---@param other mat4
---@return mat4
function mat4:add(other)
	local ret = mat4.new()
	for i=0,3 do
		for j=0,3 do
			ret.data[i][j] = self.data[i][j] + other.data[i][j]
		end
	end
	return ret
end
mat4.__add = mat4.add

---
---@param other mat4
---@return mat4
function mat4:sub(other)
	local ret = mat4.new()
	for i=0,3 do
		for j=0,3 do
			ret.data[i][j] = self.data[i][j] - other.data[i][j]
		end
	end
	return ret
end
mat4.__sub = mat4.sub

function mat4:matmul(other)
	local ret = mat4.new()
	for i=0,3 do
		for j=0,3 do
			for k=0,3 do
				ret.data[i][j] = ret.data[i][j] + self.data[i][k]*other.data[k][j]
			end
		end
	end
	return ret
end

function mat4:scalarmul(other)
	local ret = mat4.new()
	for i=0,3 do
		for j=0,3 do
			ret.data[i][j] = other*self.data[i][j]
		end
	end
	return ret
end

---comment
---@param other mat4|number
---@return mat4
function mat4:mul(other)
	if type(other) == 'number' then
		return self:scalarmul(other)
	elseif type(self) == 'number' then
		return other:scalarmul(self)
	else
		return self:matmul(other)
	end
end
mat4.__mul = mat4.mul

---
---@param other mat4|number
---@return mat4
function mat4:div(other)
	local ret = mat4.new()
	if type(other) == 'number' then
		for i=0,3 do
			for j=0,3 do
				ret.data[i][j] = self.data[i][j] / other
			end
		end
	elseif type(self) == 'number' then
		for i=0,3 do
			for j=0,3 do
				ret.data[i][j] = self / other.data[i][j]
			end
		end
	else
		for i=0,3 do
			for j=0,3 do
				ret.data[i][j] = self.data[i][j] / other.data[i][j]
			end
		end
	end
	return ret
end
mat4.__div = mat4.div

function mat4:T()
	local ret = mat4.new()
	for i=0,3 do
		for j=0,3 do
			ret.data[i][j] = self.data[j][i]
		end
	end
	return ret
end

---Returns maximum value and its position
---@return number maximumNumber
---@return integer rowPosition
---@return integer colPosition
function mat4:max()
	local maximum = -math.huge
	local maxIndexI, maxIndexJ = 0, 0
	for i=0,3 do
		for j=0,3 do
			if self.data[i][j] > maximum then
				maximum = self.data[i][j]
				maxIndexI = i
				maxIndexJ = j
			end
		end
	end
	return maximum, maxIndexI, maxIndexJ
end

---Returns minimum value and its position
---@return number minimumNumber
---@return integer rowPosition
---@return integer colPosition
function mat4:min()
	local maximum = math.huge
	local maxIndexI, maxIndexJ = 0, 0
	for i=0,3 do
		for j=0,3 do
			if self.data[i][j] < maximum then
				maximum = self.data[i][j]
				maxIndexI = i
				maxIndexJ = j
			end
		end
	end
	return maximum, maxIndexI, maxIndexJ
end

---Creates a rotation matrix
---@param angle number
---@param axis? '"X"'|'"Y"'|'"Z"'
function mat4.rotation(angle, axis)
	axis = axis or 'Z'
	if axis == 'X' then
		return mat4.new{{
			{1, 0, 0, 0},
			{0, cos(angle), sin(angle), 0},
			{0, -sin(angle), cos(angle), 0},
			{0, 0, 0, 1}
		}}
	elseif axis == 'Y' then
		return mat4.new{{
			{cos(angle), 0, -sin(angle), 0},
			{0, 1, 0, 0},
			{sin(angle), 0, cos(angle), 0},
			{0, 0, 0, 1}
		}}
	elseif axis == 'Z' then
		return mat4.new{{
			{cos(angle), sin(angle), 0, 0},
			{-sin(angle), cos(angle), 0, 0},
			{0, 0, 1, 0},
			{0, 0, 0, 1}
		}}
	end
end

---Creates a matrix from a point
---@param x number
---@param y number
---@param z? number
---@return mat4
function mat4.point(x, y, z)
	return mat4.new{{
		{x, y, z or 0, 1}
	}}
end

local function determinantHelper(mat)
	if #mat == 4 then
		return
			mat[1][1]*determinantHelper{{mat[2][2], mat[2][3], mat[2][4]}, {mat[3][2], mat[3][3], mat[3][4]}, {mat[4][2], mat[4][3], mat[4][4]}} -
			mat[1][2]*determinantHelper{{mat[2][1], mat[2][3], mat[2][4]}, {mat[3][1], mat[3][3], mat[3][4]}, {mat[4][1], mat[4][3], mat[4][4]}} +
			mat[1][3]*determinantHelper{{mat[2][1], mat[2][2], mat[2][4]}, {mat[3][1], mat[3][2], mat[3][4]}, {mat[4][1], mat[4][2], mat[4][4]}} -
			mat[1][4]*determinantHelper{{mat[2][1], mat[2][2], mat[2][3]}, {mat[3][1], mat[3][2], mat[3][3]}, {mat[4][1], mat[4][2], mat[4][3]}}
	elseif #mat == 2 then
		return mat[1][1]*mat[2][2] - mat[2][1]*mat[1][2]
	elseif #mat == 3 then
		return
			mat[1][1]*determinantHelper{{mat[2][2], mat[2][3]}, {mat[3][2], mat[3][3]}} -
			mat[1][2]*determinantHelper{{mat[2][1], mat[2][3]}, {mat[3][1], mat[3][3]}} +
			mat[1][3]*determinantHelper{{mat[2][1], mat[2][2]}, {mat[3][1], mat[3][2]}}
	end
end

function mat4:determinant()
	local mat = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}}
	for i=0,3 do
		for j=0,3 do
			mat[i+1][j+1] = self.data[i][j]
		end
	end

	return determinantHelper(mat)
end

function mat4:toPoint()
	return self.data[0][0], self.data[0][1], self.data[0][2]
end

function mat4:__tostring()
	local colLengths = {[0] = 0, 0, 0, 0}
	for i=0,3 do
		for j=0,3 do
			local length = #(('%g'):format(self.data[i][j]))
			if length > colLengths[j] then
				colLengths[j] = length
			end
		end
	end
	for i=0,3 do
		colLengths[i] = ('%%%dg '):format(colLengths[i])
	end
	local buf = {'[\n'}
	for i=0,3 do
		table.insert(buf, '   [ ')
		for j=0,3 do
			table.insert(buf, colLengths[j]:format(self.data[i][j]))
		end
		table.insert(buf, ']\n')
	end
	table.insert(buf, ']')
	return table.concat(buf)
end

mat4.new = ffi.metatype('mat4', mat4) ---@type fun(values?: number[][]): mat4

mat4.identity = mat4.new{{
	{1,0,0,0},
	{0,1,0,0},
	{0,0,1,0},
	{0,0,0,1}
}}

return mat4
